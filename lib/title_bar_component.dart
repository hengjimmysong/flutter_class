import 'package:flutter/material.dart';

class TitleBar extends StatelessWidget {
  TitleBar({
    this.title = "Placeholder",
    this.height = 30.0
  });

  String title;
  double height;

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.green,
        width: double.infinity,
        height: height,
        margin: EdgeInsets.only(bottom: 10.0),
        child: Center(
            child: Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        )));
  }
}
