import 'package:flutter/material.dart';
import 'package:ui_app/custom_colors.dart';
import 'package:ui_app/pages/home_page/home_page.dart';
import 'package:ui_app/title_bar_component.dart';

class TestPage extends StatefulWidget {
  TestPage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _TestPageState createState() => new _TestPageState();
}

class _TestPageState extends State<TestPage> {
  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    Text(
      'Cycle11111',
    ),
    Container(
        child: Text(
      'cycle 2',
    ))
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the TestPage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(widget.title),
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
          // child: _widgetOptions.elementAt(_selectedIndex),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  RaisedButton(
                    child: Text('Cycle 1'),
                    onPressed: () {
                      _onItemTapped(0);
                    },
                    color: (() {
                      if (_selectedIndex == 0) {
                        return CustomColors.PRIMARY_COLOR;
                      } else {
                        return Colors.white;
                      }
                    }()),
                  ),
                  RaisedButton(
                    child: Text('Cycle 2'),
                    onPressed: () {
                      _onItemTapped(1);
                    },
                    color: (() {
                      if (_selectedIndex == 1) {
                        return CustomColors.PRIMARY_COLOR;
                      } else {
                        return Colors.white;
                      }
                    }()),
                  )
                ],
              ),
              Container(
                child: _widgetOptions.elementAt(_selectedIndex),
              )
            ],
          )),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            title: Text('Business'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.school),
            title: Text('School'),
          ),
        ],
        currentIndex: _selectedIndex,
        // selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
