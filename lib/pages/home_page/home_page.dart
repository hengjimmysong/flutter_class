import 'package:flutter/material.dart';
import 'package:ui_app/title_bar_component.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key, this.title});

  String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: new Text(title),
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 30.0),
          child: Column(
            children: <Widget>[
              new TitleBar(
                title: 'Home Page',
              ),
              RaisedButton(
                  child: Text('Go to Contact Us'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/contact-us');
                  }),
              RaisedButton(
                  child: Text('Go to Tab Page'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/tabs');
                  }),
                  RaisedButton(
                  child: Text('Go to Test Page'),
                  onPressed: () {
                    Navigator.pushNamed(context, '/test');
                  })
            ],
          )),
    );
  }
}

