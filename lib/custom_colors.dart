import 'package:flutter/material.dart';

/// App Colors Class - Resource class for storing app level color constants
class CustomColors {
  static const Color PRIMARY_COLOR = Color(0xFF009A3E);
}
