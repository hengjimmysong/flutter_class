import 'package:flutter/material.dart';
import 'package:ui_app/pages/contact_us_page/contact_us_page.dart';
import 'package:ui_app/pages/home_page/home_page.dart';
import 'package:ui_app/pages/tab_pages/tab_pages.dart';
import 'package:ui_app/pages/test_page/test_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: new HomePage(title: 'Home Page'),
      routes: <String, WidgetBuilder> {
      '/contact-us': (BuildContext context) => ContactUsPage(title: 'Contact Us'),
      '/tabs': (BuildContext context) => TabPage(title: 'Tab Page'),
      '/test': (BuildContext context) => TestPage(title: 'Test Page'),
    },
    );
  }
}

